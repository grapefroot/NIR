#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 13:38:14 2016

@author: grapefroot
"""

import pandas as pd
import lasagne 
import theano
import theano.tensor as T
import time
import numpy as np
import pickle as pp

from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA

from utils import iterate_minibatches, load_data, load_merged_data, load_merged_data_SVD, split_data, get_weights

def build_network(input_var=None):
    l_in = lasagne.layers.InputLayer(shape=(None, ),
                                     input_var=input_var, 
                                     name = 'inputLayer')
    l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=131, output_size=50)
    
    l_reconstruction = lasagne.layers.EmbeddingLayer(l_emb, input_size=50, output_size=131)
    
    return l_reconstruction    
    

def build_dense_network(input_var=None,dimensions=20):
    l_in = lasagne.layers.InputLayer(shape=(None, 134),
                                     input_var=input_var,
                                     name='simple_network_layer')
    
    l_emb = lasagne.layers.DenseLayer(l_in,
                                      num_units=dimensions,
                                      nonlinearity=lasagne.nonlinearities.rectify)
    
    l_out = lasagne.layers.DenseLayer(l_emb,
                                      num_units=134,
                                      nonlinearity = lasagne.nonlinearities.identity)
    return l_out

def train_enc(num_epochs=1000, dimensions=40):
    
    
    x, y = load_merged_data()
    X_train, X_val, X_test = split_data(x)
    
    input_var = T.matrix('inputs')
    target_var = T.fmatrix('targets')
    
    network = build_dense_network(input_var, dimensions=dimensions)
    
    prediction = lasagne.layers.get_output(network)
    
    loss = lasagne.objectives.squared_error(prediction, target_var)
    loss = loss.mean()
    params = lasagne.layers.get_all_params(network, trainable=True)
    updates = lasagne.updates.nesterov_momentum(loss,
                                    params,
                                    learning_rate=0.01,
                                    momentum=0.9)

    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.squared_error(test_prediction, target_var)
    test_loss = test_loss.mean()
    
    
    train_fn = theano.function([input_var, target_var], loss, updates=updates)
    
    
    val_fn = theano.function([input_var, target_var], test_loss)
    
    
    print("Starting training...")
    print("Num epochs " + str(num_epochs))
    
    train_errs = []
    val_errs = []

    for epoch in range(num_epochs):
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, X_train, 500, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1
        train_errs.append(train_err)
            
        val_err = 0
        val_batches = 0
        for batch in iterate_minibatches(X_val, X_val, 50, shuffle=False):
            inputs, targets = batch
            val_err += val_fn(inputs, targets)        
            val_batches += 1
        val_errs.append(val_err)
            
            
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, num_epochs, time.time() - start_time))
        
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))    
    
    return network, train_errs, val_errs    

def run_plot_enc():    
    number_of_epochs = input('enter number of epochs: ')
    network, train_errs, val_errs = train_enc(number_of_epochs)
    weights = get_weights(network)
    plot = plt.figure(figsize=(10, 10))
    plt.plot(train_errs)
    plt.plot(val_errs)
    plot.show()
    
def encode(W, biases):
    assert W.shape[1] == biases.shape[0]
    return lambda x: lasagne.nonlinearities.rectify(np.dot(W.T, x) + biases )
    
def get_encode_func(dimensions, train_new=False, save=False):
    if train_new:
        network, _, _ = train_enc(dimensions = dimensions)
        weights = get_weights(network=network)
        if save:
            np.save('emb_{}'.format(dimensions), (weights[0], weights[1]))
        weights, biases = weights[0], weights[1]
    else:
        weights, biases = np.load('emb_{}.npy'.format(dimensions))
        #load from 
    
    return encode(W=weights, biases=biases)
        

