#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  2 17:27:12 2016

@author: grapefroot
"""
import pandas as pd
import lasagne 
import theano
import theano.tensor as T
import time
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]    


def build_function():
    x = T.imatrix()
    network = build_dense_network()
    output = lasagne.layers.get_output(network, x)
    function = theano.function([x], output)
    return function
     
def load_data(train_size=0.9):
    to_embed = pd.read_csv('./td80_w_ctags.csv')
    to_embed = to_embed[400:]
    to_embed.drop('time', axis=1, inplace=True)
    array = np.array(to_embed, dtype=np.float32)
    scaled = StandardScaler().fit_transform(array)
    train_size=int(math.floor(train_size*scaled.shape[0]))
    X_train = scaled[:train_size]
    val_size = train_size - scaled.shape[0]
    assert val_size < 0
    X_val = scaled[val_size:]
    return X_train, X_val
    
def split_data(dataset, labels=None, train_size=0.9):
    train_size=int(math.floor(train_size*dataset.shape[0]))
    X_train = dataset[:train_size]
    val_size = train_size - dataset.shape[0]
    test_size = -100
    assert val_size < 0
    X_val = dataset[val_size:]
    X_test = X_val[test_size:] 
    if labels is None:
        return X_train, X_val, X_test
    else:
        y_train = labels[:train_size]
        y_val = labels[val_size:]
        y_test = y_val[test_size:]
        return X_train, X_val, X_test, y_train, y_val, y_test
               
    
def load_merged_data():
    to_embed = pd.read_csv('./td80_w_ctags.csv')
    to_embed['time'] = to_embed['time'].apply(lambda x: pd.to_datetime(x))
    features = pd.read_csv('./djia.csv')
    features['time'] = features['time'].apply(lambda x: pd.to_datetime(x))
    dataset = pd.merge(left=to_embed, 
                       right=features, 
                       how='inner', 
                       left_on='time', 
                       right_on='time')
    to_label = np.array(dataset[['close_price', 'open_price']])
    labels = []
    for i in range(1, to_label.shape[0]):
        labels.append(int((to_label[i][1] - to_label[i-1][0] ) >= 0))
    labels = np.array(labels, dtype=np.int32)
    dataset.drop(0, axis=0, inplace=True)
    dataset.drop('close_price', axis=1, inplace = True)
    dataset.drop('open_price', axis=1, inplace = True)
    dataset.drop('time', axis=1, inplace=True)
    assert dataset.shape[0] == labels.shape[0]
    dataset = np.array(dataset, dtype=np.float32)
    scaler = MinMaxScaler(feature_range=(0,1), copy=False)
    scaler.fit_transform(dataset)
    return dataset, labels
   
def load_merged_data_SVD(n_components):
    data, labels = load_merged_data()
    pca = PCA(n_components=n_components)
    transformed = pca.fit_transform(data)
    return split_data(transformed, labels, train_size=0.9)
    
def get_weights(network):
    return lasagne.layers.get_all_param_values(network)
