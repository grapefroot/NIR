# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import pandas as pd
import lasagne 
import theano
import theano.tensor as T
from itertools import product
import time
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt

from utils import iterate_minibatches, load_data, load_merged_data, load_merged_data_SVD, split_data, get_weights 
from encoder import get_encode_func
    
def build_classification_network(input_var=None, input_dimensions=134,
                                 num_units_1=10, num_units_2=10, num_units_3=10):
    l_in = lasagne.layers.InputLayer(shape=(None, input_dimensions), 
                                     input_var=input_var,
                                     name='input layer')
    
    l_drp1 = lasagne.layers.DropoutLayer(l_in, p=0.5)
    
    l_fcc1 = lasagne.layers.DenseLayer(l_drp1,
                                       num_units=num_units_1,
                                       nonlinearity=lasagne.nonlinearities.rectify,
                                       name='fully connected 1',
                                       W=lasagne.init.Orthogonal('relu')) 
    
    l_drp2 = lasagne.layers.DropoutLayer(l_fcc1, p=0.5)
    
    l_fcc2 = lasagne.layers.DenseLayer(l_drp2,
                                       num_units=num_units_2,
                                       nonlinearity=lasagne.nonlinearities.rectify,
                                       name='fully connected 2',
                                       W=lasagne.init.Orthogonal('relu'))
    
    l_fcc3 = lasagne.layers.DenseLayer(l_fcc2,
                                       num_units=num_units_3,
                                       nonlinearity=lasagne.nonlinearities.rectify,
                                       W=lasagne.init.Orthogonal('relu'))
    
    
    l_out = lasagne.layers.DenseLayer(l_fcc3,
                                      num_units=2,
                                      nonlinearity=lasagne.nonlinearities.softmax)
    return l_out
    
def build_custom_mlp(input_var=None, input_dimensions=50, depth=2, width=10, drop_input=.2, drop_hidden=.5):
    network = lasagne.layers.InputLayer(shape=(None, input_dimensions),
                                        input_var=input_var)
    if drop_input:
        network = lasagne.layers.dropout(network, p=drop_input)
    nonlin = lasagne.nonlinearities.rectify
    for _ in range(depth):
        network = lasagne.layers.DenseLayer(
                network, width, nonlinearity=nonlin)
        if drop_hidden:
            network = lasagne.layers.dropout(network, p=drop_hidden)
    # Output layer:
    softmax = lasagne.nonlinearities.softmax
    network = lasagne.layers.DenseLayer(network, 2, nonlinearity=softmax)
    return network

    
def train_clf(build_network_architecture=None, num_epochs=500, dimensions=50):
    encoder_function = get_encode_func(dimensions=dimensions, train_new=False, save=False)
    x, y = load_merged_data()
    x = np.apply_along_axis(encoder_function, 1, x)
    X_train, X_val, X_test, y_train, y_val, y_test = split_data(x, y, train_size=0.75)
    
    input_var = T.matrix('inputs')
    target_var = T.ivector('labels')
    
    network=build_network_architecture(input_var)#build_classification_network(input_var, input_dimensions=x.shape[1], num_units_1=X, num_units_2=Y)
    
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
    loss = loss.mean()
    
    params = lasagne.layers.get_all_params(network, trainable=True)
    updates = lasagne.updates.nesterov_momentum(
            loss, params, learning_rate=0.05, momentum=0.9)

    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.categorical_crossentropy(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()
    
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var),
                      dtype=theano.config.floatX)

    train_fn = theano.function([input_var, target_var], loss, updates=updates)

    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])

    print("Starting training...")
    print("Num epochs " + str(num_epochs))
    
    train_errs = []
    val_errs = []
    val_max_acc = 0
    val_max_epoch = 0
    for epoch in range(num_epochs):
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(X_train, y_train, 500, shuffle=True):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1
        train_errs.append(train_err)

        val_err = 0
        val_acc = 0
        val_batches = 0
        
        for batch in iterate_minibatches(X_val, y_val, 100, shuffle=False):
            inputs, targets = batch
            err, acc = val_fn(inputs, targets)
            val_err += err
            val_acc += acc
            val_batches += 1
        val_errs.append(val_err)

        
        epoch_acc = val_acc / val_batches * 100
     #   print("Epoch {} of {} took {:.3f}s".format(
     #       epoch + 1, num_epochs, time.time() - start_time))
     #   print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
     #   print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
     #   print("  validation accuracy:\t\t{:.2f} %".format(epoch_acc))
    
        
        if epoch_acc > val_max_acc:
                val_max_acc = epoch_acc
                val_max_epoch = epoch
    
    test_err = 0
    test_acc = 0
    test_batches = 0
    for batch in iterate_minibatches(X_test, y_test, 50, shuffle=False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
        test_batches += 1
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  test accuracy:\t\t{:.2f} %".format(
        test_acc / test_batches * 100))
    print(" max val acc: {} on epoch {}".format(val_max_acc, val_max_epoch))
    
        
    return network, train_errs, val_errs, epoch_acc    

def run_save_csv(X, Y, num_epochs, num_dims):    
    network, val_acc, test_acc = train_clf(X, Y, num_epochs, num_dims)
    val_end, test_end = val_acc[-1], test_acc[-1]
    return X, Y, num_epochs, num_dims, val_end, test_end
    

def run_clf(param_dict, n_times=1):
    epochs_list = param_dict['num_epochs']
    if len(epochs_list) == 0:
        num_epochs = input('enter number of epochs: ')
        epochs_list.append(num_epochs)
    
    dimensions_list = param_dict['num_dimensions']    
    if len(dimensions_list) == 0:
        num_dimensions = input('enter number of dimensions: ')
        dimensions_list.append(num_dimensions)
     
    num_units1_list = param_dict['num_units1']
    if len(num_units1_list) == 0:
        num_units_1 = input('enter number of num_units_1: ')
        num_units1_list.append(num_units_1)
        
    num_units2_list = param_dict['num_units2']    
    if len(num_units2_list) == 0:
        num_units_2 = input('enter number of num_units_2: ')
        num_units2_list.append(num_units_2)
        
    num_units3_list = param_dict['num_units2']    
    if len(num_units3_list) == 0:
        num_units_3 = input('enter number of num_units_3: ')
        num_units3_list.append(num_units_3)    

    
    report = map(lambda (X, Y, ne, nd): run_save_csv(X, Y, ne, nd), np.repeat(product(num_units1_list, num_units2_list, num_units3_list, epochs_list, dimensions_list), n_times))
    data = pd.DataFrame(data = report, columns=['X', 'Y', 'num_epochs', 'num_dimensions', 'val_acc', 'train_acc'])
    data.to_csv('./results_{}'.format(datetime.now()))

  #  network, train_errs, val_errs = train_clf(number_of_epochs, number_of_dimensions)
  #  plot = plt.figure(figsize=(10, 10))
  #  plt.plot(train_errs, label='train')
  #  plt.plot(val_errs, label='validation')
  #  plot.show()    
  #  plt.legend()
  
def build_arch_for_parameters(depth, width):
    return lambda x: build_custom_mlp(x, input_dimensions=5, depth=depth, width=width, drop_input=.2, drop_hidden=.5)        

def create_entry(depth, width):
    return "{}x{}".format(depth, width), build_arch_for_parameters(depth, width)

from itertools import product
        
def create_dictionary():
    depths = range(1,6)
    widths = range(10, 110, 10)
    return dict(map(lambda (x, y): create_entry(x,y), list(product(depths, widths))))
  

def compute_hist_for_arch(network_last_layer, n_rounds=50):
    train_errs_for_arch = []
    val_errs_for_arch = []
    accs = []
    for i in range(n_rounds):
        _, train_errs, val_errs, acc = train_clf(build_network_architecture=network_last_layer, num_epochs=1000, dimensions=5)
        last_epoch_train_error, last_epoch_val_error= train_errs[-1], val_errs[-1]
        train_errs_for_arch.append(last_epoch_train_error)
        val_errs_for_arch.append(last_epoch_val_error)
	accs.append(acc)
    return  train_errs_for_arch, val_errs_for_arch, accs

def compute_hist_for_arch_dict(arch_dict):
    for description, arch in arch_dict.items():
        terr, verr, acc = compute_hist_for_arch(network_last_layer=arch)
        plt.clf()
        plt.figure(figsize=(30,10))
        
        plt.subplot(131)
        plt.hist(terr, bins=10)
	plt.ylim([0, 10])
        
        plt.subplot(132)
        plt.hist(verr, bins=10)
	plt.ylim([0, 10])

	plt.subplot(133)
	#optimistic
	plt.hist(acc, bins=10, align='mid', range=[45,80])
        
        plt.savefig(description + '.png')
    
    
        
        
if __name__ == '__main__':
   # param_dict = {
   #             'num_epochs' : range(1, 100), 
   #             'num_dimensions' : range(5,40),
   #             'num_units1' : range(2, 10),
   #             'num_units2' : range(2, 10),
   #             'num_units3'  : range(2 , 10)
   #             }
   # run_clf(param_dict, 20)
   compute_hist_for_arch_dict(create_dictionary())
